# HTML cleaner
Web app for html cleaning built with _Flask_ framework.
![hclean-web screenshot](screenshot.jpg)


### To run locally
The app is Dockerized. To run it locally, please have `docker` and `docker-compose` installed.
```bash
cd hclean-web
docker-compose build
docker-compose up
```


### Setting up GitLab runner
```bash
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
apt-get install gitlab-runner
apt install docker docker-compose
```
